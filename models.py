# Copyright (c) 2023 Florian Fischer. All rights reserved.
#
# This file is part of geldschieberbot.
#
# geldschieberbot is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# geldschieberbot is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# geldschieberbot found in the LICENSE file. If not,
# see <https://www.gnu.org/licenses/>.
"""Models used by geldschieberbot"""

import json
import typing as T

from dataclasses import dataclass, field

from utils import to_euro


@dataclass
class MessageContext:
    """Class representing the context of a message passed to a command function"""
    sender_number: str
    sender: T.Optional[str]
    args: list[str]
    body: list[str]
    timestamp: str


@dataclass
class Modification:
    """Class representing a single modification to the balance

    Amount is transfered from donor to the recipient.
    """
    recipient: str
    donor: str
    amount: int

    def in_string(self) -> str:
        """Format the change using the recipient as initiator"""
        return (f'{self.recipient} {"->" if self.amount < 0 else "<-"}'
                f' {to_euro(abs(self.amount))} {self.donor}')

    def out_string(self) -> str:
        """Format the change using the donor as initiator"""
        return (f'{self.donor} {"->" if self.amount < 0 else "<-"}'
                f' {to_euro(abs(self.amount))} {self.recipient}')


@dataclass
class Change:
    """Class representing a change to the state caused by a single command"""
    cmd: list[str]
    modifications: list[Modification]
    timestamp: str
    rewind_cmds: list[list[str]] = field(default_factory=lambda: [])


@dataclass
class Quote:
    """Class representing a message to quote"""
    timestamp: str
    author: str


class GeldschieberbotJSONEncoder(json.JSONEncoder):
    """Custom JSONEncoder supporting our dataclasses"""

    def default(self, o):
        if isinstance(o, (Modification, Change)):
            return o.__dict__
        return json.JSONEncoder.default(self, o)


@dataclass
class Reply:
    """Class representing a reply from the bot"""
    msg: str
    attachment: T.Optional[str] = None
