#!/bin/bash

# Source config.sh if it is available
[[ -f config.sh ]] && source config.sh

# Create dir for all your received messages
mkdir -p msgs

# Is GSB_USER set ?
test -z "$GSB_USER" && >&2 echo "GSB_USER not set!" && exit 1

old_week=$(date +%U)
week=$(date +%U)

while [ true ]
do
	sleep 30

	d=$(date -Ins)
	week=$(date +%U)

	signal-cli --output=json -u ${GSB_USER} receive | tee msgs/msg${d}.log | python3 single_shot.py

	# only keep non empty logs
	[[ $(du msgs/msg${d}.log | cut -f 1) == 0 ]] && rm msgs/msg${d}.log

	# make a tar for every week
	if [[ ${old_week} != ${week} ]]
	then
		tar cfz msgs/msgs$(date +%U-%g).tar.gz msgs/msg*.log && rm msgs/msg*.log && old_week=${week}
	fi
done
