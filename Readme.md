# Geldschieberbot

Geldschieberbot is a python script to bookkeep the finances of a group.
It is intended to be run on the output of signal-cli. But it should parse
other input without a lot of modification.

## Configuration

Geldschieberbot is configured with a set of Environment variables:

* GSB_GROUP_ID the signal group_id you want to serve
* GSB_STORE_PATH the directory where Geldschieberbot stores the balance and the useres
* GSB_SEND_CMD the command Geldschieberbot uses to send replies
* GSB_USER the mobile number signal-cli should receive messages with

Copy config.sh.sample to config.sh and edit it to match your environment.

## Usage

If you have your config.sh in place you can just run
`./run.sh`
and your bot should be up and serving your needs.

Happy Geldschieben!

## License

Geldschieberbot is licensed under the terms of the GNU General Public License
version 3 or later [GPL-3.0-or-later](https://www.gnu.org/licenses/#GPL).
