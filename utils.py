# Copyright (c) 2023 Florian Fischer. All rights reserved.
#
# This file is part of geldschieberbot.
#
# geldschieberbot is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# geldschieberbot is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# geldschieberbot found in the LICENSE file. If not,
# see <https://www.gnu.org/licenses/>.
"""Utility functions used by the geldschieberbot"""


def to_cent(euro) -> int:
    """Parse string containing euros into a cent value"""
    if '.' in euro:
        euro = euro.split('.')
    else:
        euro = euro.split(',')
    if len(euro) > 2:
        raise TypeError
    euro[0] = int(euro[0])
    if len(euro) < 2:
        euro.append(0)
    else:
        if len(euro[1]) == 1:
            euro[1] = int(euro[1]) * 10
        else:
            euro[1] = int(euro[1])
    amount = euro[0] * 100 + euro[1]
    if amount < 0:
        raise ValueError
    return amount


def to_euro(cents) -> str:
    """Format cents as euro"""
    return f"{cents/100:.2f}"
