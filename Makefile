SRC=$(wildcard *.py)

.PHONY: check
check: check-pylint check-format check-mypy

.PHONY: check-pylint
check-pylint:
	pylint --rcfile=.pylint.rc -j 0 $(SRC) || ./tools/check-pylint $$?

.PHONY: check-format
check-format:
	yapf -d $(SRC)

.PHONY: check-format
check-mypy:
	mypy $(SRC)

.PHONY: format
format:
	yapf -i $(SRC)

.PHONY: test
test:
	python3 test.py
